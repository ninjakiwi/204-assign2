import java.net.*;
import java.io.*;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ExecutorService;

import java.util.concurrent.TimeUnit;

class HttpServer {
	private static final ExecutorService threadPool = Executors.newFixedThreadPool(4);

	public static void main(String[] args) {
		System.out.println("Web server starting");

		try {
			ServerSocket serverSocket = new ServerSocket(8181);
		    System.out.println("Web server started on port " + serverSocket.getLocalPort());
		    
			while (true) {
				Socket client = serverSocket.accept();
				HttpServerSession newTask = new HttpServerSession(client);
				threadPool.execute(newTask);
			}
		} catch (Exception ex) {
			System.out.println("Server crashed, exception: " + ex);
		}
	}
}

class HttpServerSession implements Runnable {
	// Time in seconds to wait for a request before closing the connection
	private static final int REQUESTTIMEOUT = 10;

	Socket client;
	long uid;
	String clientName;

	@Override
	public void run() {
		uid = Thread.currentThread().getId();
		clientName = client.getInetAddress().getHostAddress();
		//System.out.println(uid + ": Connected to client: " + clientName);

		// Fetch the input stream
		BufferedReader reader;
		try {
			reader = new BufferedReader(new InputStreamReader(client.getInputStream()));
		} catch (IOException ex) {
			endSession("Failed to fetch reader for \"" + clientName + "\", aborting connection. Exception: " + ex);
			return;
		}

        // Attempt to get reqeuest and return it
		try {
			// Get first line in the header, close connection if it doesn't exist
			String clientRequestHeader = reader.readLine();
			if (clientRequestHeader == null) {
				endSession("Missing header, aborting connection.");
				return;
			}

			// Check first line of the header consists of 3 parts and it is a GET request
			String[] crhParts = clientRequestHeader.split(" ");
			if (crhParts.length != 3 || crhParts[0].compareTo("GET") != 0) {
				endSession("Invalid header: " + clientRequestHeader + ", aborting connection.");
				return;
			}

			// Get the file requested by the client
			String filename = crhParts[1].substring(1);
			System.out.println(uid + ": \"" + clientName + "\" Getting File: " + filename);

			// Try index.html if the filename is blank
			if (filename == "") filename = "index.html";
			File file = new File(filename);

			// Fetch the output stream
			OutputStream outStream = client.getOutputStream();

			// If file doesn't exist, send 404 and close connection
			if (!file.exists()) {
				writeNotFound(outStream);
				outStream.close();
				endSession("File requested by \"" + clientName + "\" does not exist: " + filename);
				return;
			}

			// Write headers and then the content for the specified file
			writeHeaders(outStream, file);
			writefile(outStream, file);

			outStream.close();

		} catch (IOException ex) {
			// Failed to get request
			System.out.println(uid + ": Session IO exception: " + ex);
		}

		// Communication finished, attempt to close the socket
		endSession();
	}

	private void writeNotFound(OutputStream outStream) throws IOException {
		// Use writer as it ensures consistant encoding for plain text
		BufferedWriter outWriter = new BufferedWriter(new OutputStreamWriter(outStream));

		writeln(outWriter, "HTTP/1.1 404 Not Found");
		writeln(outWriter, "server: simpleserver");
		writeln(outWriter, "");
		outWriter.flush();
	}

	private void writeHeaders(OutputStream outStream, File file) throws IOException {
		// Use writer as it ensures consistant encoding for plain text
		BufferedWriter outWriter = new BufferedWriter(new OutputStreamWriter(outStream));

		writeln(outWriter, "HTTP/1.1 200 OK");
		writeln(outWriter, "Server: simpleserver");
		writeln(outWriter, "Content-Length: " + Long.toString(file.length()));

		// Write content type (plain text if not one list below)
		String filename = file.getName();
		String[] splitFilename = filename.split("\\.");
		if (splitFilename.length > 1) {
			switch (splitFilename[splitFilename.length - 1]) {
				case "html":
					writeln(outWriter, "content-type: text/html");
					break;
				case "js":
					writeln(outWriter, "content-type: application/javascript");
					break;
				case "css":
					writeln(outWriter, "content-type: text/css");
					break;
				case "png":
					writeln(outWriter, "content-type: image/png");
					break;
				case "jpeg":
				case "jpg":
					writeln(outWriter, "content-type: image/jpeg");
					break;
				default:
					writeln(outWriter, "content-type: text/plain");
			}
		} else {
			writeln(outWriter, "content-type: text/plain");
		}

		writeln(outWriter, "");
		outWriter.flush();
	}

	private void writeln(BufferedWriter outWriter, String data) throws IOException {
		String finalData = data + "\r\n";
		byte[] dataBytes = finalData.getBytes();
		for (byte thing : dataBytes) {
			outWriter.write(thing);
		}
		outWriter.flush();
	}

	private void writefile(OutputStream outStream, File file) throws IOException {
		byte[] buffer = new byte[1024];
		FileInputStream fileStream = new FileInputStream(file);
		BufferedOutputStream buffero = new BufferedOutputStream(outStream, 1024);

        while ((fileStream.read(buffer))!= -1)
        {
        	// Delay for slow server simulation
			try {
				TimeUnit.MILLISECONDS.sleep(10);
			} catch (Exception ex) {}

            buffero.write(buffer);
            buffero.flush();
        }

        buffero.close();
		fileStream.close();
	}

	/// Print msg and close the socket
	private void endSession(String msg) {
		System.out.println(uid + ": " + msg);
		endSession();
	}

	private void endSession() {
		try {
			client.close();
		} catch (IOException ex) {
			System.out.println(uid + ": Failed to close connection: " + ex);
		}
	}

	public HttpServerSession(Socket socket) {
		client = socket;
	}
}
